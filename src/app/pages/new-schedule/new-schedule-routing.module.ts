import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NewSchedulePage } from './new-schedule';

const routes: Routes = [
  {
    path: '',
    component: NewSchedulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewScheduleRoutingModule { }
