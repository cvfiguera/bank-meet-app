import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { DocumentsPage } from "./documents";

const routes: Routes = [
  {
    path: "document",
    component: DocumentsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentsPageRoutingModule {}
