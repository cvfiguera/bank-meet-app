import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { DocumentsPage } from './documents';
import { DocumentsPageRoutingModule } from './documents-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocumentsPageRoutingModule
  ],
  declarations: [
    DocumentsPage
  ],
  entryComponents: [

  ]
})
export class DocumentsModule { }
