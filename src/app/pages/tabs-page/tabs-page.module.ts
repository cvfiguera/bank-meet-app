import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";

import { TabsPage } from "./tabs-page";
import { TabsPageRoutingModule } from "./tabs-page-routing.module";

import { AboutModule } from "../about/about.module";
import { MapModule } from "../map/map.module";
import { ScheduleModule } from "../schedule/schedule.module";
import { SessionDetailModule } from "../session-detail/session-detail.module";
import { SpeakerDetailModule } from "../speaker-detail/speaker-detail.module";
import { SpeakerListModule } from "../speaker-list/speaker-list.module";
import { DocumentsModule } from "../documents/documents.module";
import { DocumentsPageRoutingModule } from "../documents/documents-routing.module";
import { RequestListModule } from "../request-list/request-list.module";

@NgModule({
  imports: [
    AboutModule,
    CommonModule,
    IonicModule,
    MapModule,
    RequestListModule,
    SessionDetailModule,
    SpeakerDetailModule,
    SpeakerListModule,
    TabsPageRoutingModule,
    DocumentsModule,
    DocumentsPageRoutingModule,
    ScheduleModule,
  ],
  declarations: [TabsPage],
})
export class TabsModule {}
