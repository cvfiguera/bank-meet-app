import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { RequestListPage } from "./request-list";

const routes: Routes = [
  {
    path: "",
    component: RequestListPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestListRoutingModule {}
