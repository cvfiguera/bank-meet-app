import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";

import { RequestListPage } from "./request-list";
import { RequestFilterPage } from "../request-filter/request-filter";
import { RequestListRoutingModule } from "./request-list-routing.module";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, RequestListRoutingModule],
  declarations: [RequestListPage, RequestFilterPage],
  entryComponents: [RequestFilterPage],
})
export class RequestListModule {}
