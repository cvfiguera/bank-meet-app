import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RequestDetailPage } from './request-detail';

const routes: Routes = [
  {
    path: '',
    component: RequestDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestDetailPageRoutingModule { }
