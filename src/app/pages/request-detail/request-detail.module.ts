import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RequestDetailPage } from './request-detail';
import { RequestDetailPageRoutingModule } from './request-detail-routing.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    RequestDetailPageRoutingModule
  ],
  declarations: [
    RequestDetailPage,
  ]
})
export class RequestDetailModule { }
